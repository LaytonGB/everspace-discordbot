interface ChildChannel {
  id: string;
  name: string;
}

interface Channel {
  id: string;
  guild: string;
  children: ChildChannel[];
}

export { Channel };
