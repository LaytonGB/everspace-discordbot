// TODO optimize storage (don't need to store names of channels)
// TODO set _id of DB to channel snowflake

import type { ArgsOf } from "discordx";
import {
  CommandInteraction,
  MessageActionRow,
  SelectMenuInteraction,
  MessageSelectMenu,
  GuildChannel,
  Guild,
  VoiceState,
} from "discord.js";
import { Discord, Slash, SelectMenuComponent, On } from "discordx";
import { Collection, ObjectId, UpdateResult, WithId } from "mongodb";

import type { Channel } from "../data/types";
import db from "../db.js";

/**
 * @func `find(GuildChannel | channelID: string): Promise<Channel | null>` Searches for the relevent channel in the database, returning it if found.
 * @func `toggle(GuildChannel): Promise<boolean>`
 * @func `enumerate(channelName: string): string`
 * @func `fetchAllSpacious(Guild | guildID: string): Promise<WithId<Channel>[] | null>`
 * @func `fetchAllChildren(g: Guild | string): Promise<Set<channelID: string> | null>`
 * @func `add(c: GuildChannel): Promise<ObjectId | null>`
 * @func `delete(c: GuildChannel | string): Promise<boolean>`
 */
class Spacious {
  private db = db.db("everspace").collection("channels") as Collection<Channel>;
  private parentCache = new Map<string, Channel>();
  private childCache = new Map<string, string>();

  constructor() {}

  private async fetchOne(
    c: GuildChannel | string
  ): Promise<WithId<Channel> | null> {
    const cid = this.getId(c);

    try {
      return (await this.db.findOne({ id: cid })) as WithId<Channel> | null;
    } catch (err) {
      console.log(err);
      return null;
    }
  }

  private async fetchFromChild(c: string): Promise<WithId<Channel> | null> {
    console.log("trying to fetch child with id:", c);
    try {
      return (await this.db.findOne({
        children: { $elemMatch: { id: c } },
      })) as WithId<Channel> | null;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  private async addChild(c: GuildChannel, pid: string): Promise<UpdateResult> {
    this.childCache.set(c.id, pid);
    const child = { id: c.id, name: c.name };
    this.parentCache.get(pid)?.children.push(child);
    console.log("pushing channel with parent id:", pid);
    return this.db.updateOne({ id: pid }, { $push: { children: child } });
  }

  private getId(c: GuildChannel | Guild | string): string {
    let id: string;
    if ("string" === typeof c) id = c;
    else id = c.id;
    return id;
  }

  // TODO improve this
  enumerate(str: string): string {
    const reg = /([0-9]+)(?![0-9])/g;
    const match = str.match(reg);
    if (match)
      return str.replace(reg, (parseInt(match[0] || "-1") + 1).toString());
    else return str + " 1";
  }

  async toggle(c: GuildChannel): Promise<boolean | null> {
    try {
      // get variables
      const chan = this.parentCache.get(c.id);
      const mongo = this.db;
      const dbChan = await mongo.findOne({ id: c.id });

      // if exists
      if (chan || dbChan) {
        //   delete from db
        //   delete from cache
        this.endSpacious(c);
      }
      // else
      //   add to db
      //   add to cache
      else {
        this.startSpacious(c);
      }
      return !(chan || dbChan);
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async fetchAllSpacious(g: Guild | string): Promise<WithId<Channel>[] | null> {
    const gid = this.getId(g);

    try {
      return (await (
        await db.db("everspace").collection("channels").find({ guild: gid })
      ).toArray()) as WithId<Channel>[] | null;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async fetchAllChildren(g: Guild | string): Promise<Set<string> | null> {
    const gid = this.getId(g);

    try {
      const parents = await this.fetchAllSpacious(gid);
      if (!parents) return null;
      return new Set(...[parents.flatMap((x) => x.children.map((y) => y.id))]);
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async get(c: GuildChannel | string): Promise<Channel | null> {
    const cid = this.getId(c);
    console.log("GET - using id:", cid);

    let channel = this.parentCache.get(cid) as Channel | null;
    if (channel) return channel;

    const dbChannel = await this.fetchOne(cid);
    if (dbChannel) this.parentCache.set(cid, dbChannel);
    return dbChannel;
  }

  async getParent(c: GuildChannel | string): Promise<Channel | null> {
    const cid = this.getId(c);

    const channelId = this.childCache.get(cid);
    const channel = channelId && (await this.get(channelId));
    if (channel) return channel;

    const dbChannel = await this.fetchFromChild(cid);
    if (dbChannel) this.childCache.set(cid, dbChannel.id);

    return channel || dbChannel;
  }

  async getChannelOrParent(c: GuildChannel | string): Promise<Channel | null> {
    const cid = this.getId(c);

    const channel = await this.get(cid);
    if (channel) return channel;

    const parent = await this.getParent(cid);
    if (parent) return parent;

    return null;
  }

  async clone(parent: GuildChannel): Promise<GuildChannel | null> {
    console.log("CLONING CHANNEL");

    // get last child
    const children = (await this.get(parent))?.children;
    const lastChild = (children && children[children.length - 1]) || parent;
    const lastName = lastChild.name;
    // duplicate parent with enumerated name of last child
    try {
      const newChan = await parent.clone({ name: this.enumerate(lastName) });
      console.log(
        "Succeeded at adding child:",
        await this.addChild(newChan, parent.id)
      );

      return newChan;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async deleteChild(c: GuildChannel): Promise<Boolean | null> {
    const cid = this.getId(c);

    // find parent
    const parent = await this.getParent(cid);
    // remove child from child cache
    this.childCache.delete(cid);
    // remove child from parent cache
    if (!parent) return null;
    let match = parent.children.findIndex((c) => c.id === cid);
    while (match !== -1) {
      parent.children.splice(match, 1);
      match = parent.children.findIndex((c) => c.id === cid);
    }
    this.parentCache.set(parent.id, parent);
    // remove child from parent in db
    const res = await this.db.updateOne(
      { id: parent.id },
      { $pull: { children: { id: cid } } }
    );

    try {
      if (res.acknowledged) await c.delete();
      return res.acknowledged;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async startSpacious(c: GuildChannel): Promise<ObjectId | null> {
    try {
      const details: Channel = {
        id: c.id,
        guild: c.guild.id,
        children: [],
      };
      const mongo = this.db;
      const res = await mongo.insertOne(details);
      if (res.acknowledged) this.parentCache.set(c.id, details);
      return res.insertedId;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  async endSpacious(c: GuildChannel | string): Promise<boolean> {
    const cid = this.getId(c);

    try {
      const mongo = this.db;
      this.parentCache.delete(cid);
      return (await mongo.deleteOne({ id: cid })).deletedCount > 0;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}
const spacious = new Spacious();

/* -------------------------------------------------------------------------- */
/*                              Channel Listener                              */
/* -------------------------------------------------------------------------- */
@Discord()
export abstract class DiscordApp {
  @On("voiceStateUpdate")
  async onVoiceStateUpdate([oldState, newState]: ArgsOf<"voiceStateUpdate">) {
    // channels <- map oldstate and newstate to their represented channels
    // dbChannels <- filter channels by whether they're in db's
    const backupChannels = oldState?.guild.channels || newState?.guild.channels;
    const channels = (
      [
        {
          chan:
            oldState.channel &&
            (await spacious.getChannelOrParent(oldState.channel)),
          state: oldState,
        },
        {
          chan:
            newState.channel &&
            (await spacious.getChannelOrParent(newState.channel)),
          state: newState,
        },
      ].filter((c) => c.chan != null) as { chan: Channel; state: VoiceState }[]
    ).reduce(
      (obj, c) => ({ ...{ [c.chan.id]: { ...c.state, ...c.chan } }, ...obj }),
      {}
    ) as { [key: string]: VoiceState & Channel };
    // for each in dbChannels:
    for (const key in channels) {
      const c = channels[key];
      const guildChannels = c.guild.channels || backupChannels;
      // collect all children
      const children = c.children
        .slice()
        .map((c) => guildChannels.cache.get(c.id))
        .filter((chan) => !!chan) as GuildChannel[];
      // children + parent
      const series = [
        guildChannels.cache.get(c.id),
        ...children,
      ] as GuildChannel[];
      // count empty
      let emptyCount: number = series.reduce((acc, chan) => {
        if (!chan) return acc;
        return acc + (chan && chan.members.size < 1 ? 1 : 0);
      }, 0);
      console.log("empty channels: ", emptyCount);

      // if empty count is 1 stop here
      if (emptyCount === 1) return;
      // if empty count < 1:
      if (emptyCount < 1) {
        // create a room
        const parent = guildChannels.cache.get(c.id) as
          | GuildChannel
          | undefined;
        if (!parent) return;
        spacious.clone(parent);
      } else {
        // delete a room
        const firstEmptyChild = c.children.find((chan) => {
          const channel = guildChannels.cache.get(chan.id) as
            | GuildChannel
            | undefined;
          const count = channel?.members.size;
          return typeof count === "number" && count < 1;
        });
        const firstEmpty =
          firstEmptyChild &&
          (guildChannels.cache.get(firstEmptyChild.id) as
            | GuildChannel
            | undefined);
        if (firstEmpty) await spacious.deleteChild(firstEmpty);
      }
    }
  }
}

@Discord()
export abstract class buttons {
  @SelectMenuComponent("channel-menu")
  async handle(interaction: SelectMenuInteraction): Promise<unknown> {
    if (!interaction?.guild) return;

    await interaction.deferReply();

    const channels = (await interaction.guild.channels.fetch()).filter((c) =>
      c.isVoice()
    );

    // extract selected value by member
    const channelValue = interaction.values?.[0];
    const selectedChannel = channels.find((c) => c.id === channelValue);

    // if value not found
    if (!(channelValue && selectedChannel)) {
      return await interaction.followUp("invalid channel, please try again");
    }

    const isNowSpacious = await spacious.toggle(selectedChannel);

    await interaction.followUp(
      `You selected channel: ${selectedChannel?.name}\n${
        isNowSpacious
          ? "The channel will now duplicate as users join."
          : "The channel will no longer duplicate as users join."
      }
      `
    );
    return;
  }

  @Slash("space", {
    description: "automagically create space as channels fill",
  })
  async space(interaction: CommandInteraction): Promise<unknown> {
    if (!(interaction?.guild && interaction.inGuild())) {
      interaction.reply("Sorry, but you need to use this command in a server.");
      return;
    }

    await interaction.deferReply();

    // get all voice channels that are not children
    // TODO make function
    const channels = (await interaction.guild.channels.fetch())
      .filter((c) => c.isVoice())
      .map((c) => ({ label: c.name, value: c.id }))
      .filter(
        async (c) =>
          !(
            (await spacious.fetchAllChildren(interaction.guildId)) || new Set()
          ).has(c.value)
      );

    // create menu for channels
    const menu = new MessageSelectMenu()
      .addOptions(channels)
      .setCustomId("channel-menu");

    // create a row for message actions
    const buttonRow = new MessageActionRow().addComponents(menu);

    // send it
    interaction.editReply({
      content: "Select a channel!",
      components: [buttonRow],
    });
    return;
  }
}
